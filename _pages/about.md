---
layout: about
title: The MPI-SWS Operating Systems Group
subtitle: Welcome to
description: |
  Systems research for the post-Moore era

  at the intersection of software and hardware.
no_page_title: true
logo: logo.svg
banner: group-2023-aug.jpg
permalink: /

---

Our research centers on the interplay of software and hardware in post-Moore
systems — specialized systems comprising tightly integrated and co-designed
hardware and software components. We are interested in the nascent challenges in
designing, implementing, and maintaining post-Moore systems in different
application domains; starting with data center network communication and machine
learning systems. Our research develops abstractions, tooling, and methodology
for building efficient post-Moore systems and applications with manageable
complexity and cost, through modular and re-usable components.

**We envision a future where specialized, tightly integrated, and efficient
post-Moore hardware-software systems can be rapidly and efficiently prototyped
and evaluated.**

Our research is practical in nature, taking ideas all the way to full prototype
implementations, building concrete solutions to problems in several domains, and
resulting in top-tier publications alongside substantial open source releases.
