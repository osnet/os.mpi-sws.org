---
layout: default
title: "Teaching"
subtitle: "What We Teach"
description: |
  Our teaching combines conceptual understanding of computer systems
  with significant hands-on experience.
banner: group/teaching.jpg
permalink: /teaching.html
---

<section>
<div class="titlebar">
  <div class="container">
    <h2>Courses</h2>
  </div>
</div>
<div class="container" markdown="1">

List of courses offered by folks in the OS group

+ Seminar: [Reliability in Modern Cloud Systems, Summer 25](https://cms.sic.saarland/cldrel_25/)
+ Advanced Lecture: [Systems with Specialized Hardware, Winter 24/25](https://cms.sic.saarland/swish_24/)
+ Core Course: [Graduate Operating Systems, Summer 24](https://cms.sic.saarland/os_24/)
+ Seminar: [Accelerating Applications with Specialized Hardware, Winter 23/24](https://cms.sic.saarland/hwaccel_23/)
+ Core Course: [Graduate Operating Systems, Winter 21/22](https://courses.mpi-sws.org/os-ws21/)
+ Core Course: [Graduate Operating Systems, Winter 19/20](https://courses.mpi-sws.org/os-ws19/)

</div>
</section>


<section>
<div class="titlebar">
  <div class="container">
    <h2>Master and Bachelor Theses</h2>
  </div>
</div>
<div class="container" markdown="1">

Our group does host and advise students working on Bachelor and Masters theses
at Saarland University. To provide high-quality advising for students, we limit
this to projects that are more or less directly related to ongoing projects in
our group.

If you are interested in doing a project in our group, start by checking out
[our projects]({% link _pages/projects.md %}). Next send an email (with your UdS
email address) to the graduate student on the project also CC'ing Antoine,
mentioning which projects you are interested in. To learn more about what doing
a thesis in our group is like, you are welcome to reach out to any our [current
students]({% link _pages/people.md %}#intern) working on their theses.

Please note that doing a project in our group generally requires a solid
background in computer systems (distributed systems, operating systems,
architecture, networking). Our projects are also generally implementation-heavy
and do not optimize for easy high grades, but instead target interesting
research questions.

</div>
</section>
