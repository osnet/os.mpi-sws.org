---
layout: default
title: "Pre-Submission Application Review"
subtitle: "Systems PhD Pre-Submission Application Review (PAR) Program"
description: |
  The PhD PAR program offers a one-time review of a potential
  systems applicant's CV and Statement of Purpose. The review is 
  done by a PhD student in our group.
banner: group/par.jpg
permalink: /pre-application-review.html
---

<section>
<div class="titlebar">
    <div class="container">
    <h2>About the Program</h2>
    </div>
</div>
<div class="container" markdown="1">
Inspired by [Columbia University's PAR program](https://www.cs.columbia.edu/cscu-phd-par-program/), we have started our own Pre-Submission Application program aimed at promoting a diverse and welcoming intellectual environment.

While Computer Science as a field has not succeeded in ensuring everyone has access to CS research and professional opportunities, this is even worse for the Computer Systems area where such opportunities are extremely limited.

This lack of accessibility in Computer Systems leads to many systemic disadvantages, primarily, students not being fully aware of the scope of research in computer systems which in turn leads to accessing application mentoring when applying for PhD positions in computer systems.

To help alleviate this issue, we have adopted the PAR program specifically for computer systems students interested in applying to MPI-SWS to work in a systems area.
</div>
</section>

<section>
<div class=titlebar>
    <div class="container">
    <h2> Submission Instructions </h2>
    </div>
</div>
<div class="container" markdown="1">

This program is only open to applicants who are considering applying for a PhD position at MPI-SWS in the field of computer systems.
The form for submitting the CV and resume is [here](https://forms.gle/DwLJw7cv3DggGeTm9). The deadline for submitting your Statement of Purpose and CV is <b> November 10, 2023</b> with the application reviews available by <b> November 24, 2023</b>.

Please note that this is a Student-Run volunteer program. Applications will be reviewed on a first-come, first-served basis with priority given to applicants from traditionally underrepresented groups in Computer Systems.

<b>Note that this program is separate from the admissions process. The PhD students reviewing application materials are not part of the admissions process and the reviews aren't indicative of any future admission decisions.</b> For more information about the admissions process and graduate studies at MPI-SWS, please visit the [institute website](https://www.mpi-sws.org/graduate-studies/).
</div>
</section>
