types:
  inprogress:
    title: In Progress
    prefix: ""

  conference:
    title: Conference
    prefix: "In "

  workshop:
    title: Workshop
    prefix: "In "

  techreport:
    title: Tech Reports
    prefix: "In "

  poster:
    title: Posters
    prefix: "In "
  
  thesis:
    title: Theses
    prefix: ""

restypes:
  paperpdf:
    label: Paper PDF

  preprint:
    label: Preprint

  slides:
    label: Slides

  video:
    label: Video

  audio:
    label: Audio

  poster:
    label: Poster

  artifact:
    label: Artifact


publications:
  laissez_hotos:
    type: inprogress
    title: |
      Rethinking resource allocation in Post-Moore Clouds
    authors:
      - internal: tejas
      - internal: antoine
    venue: Under submission
    year: 2025
    people:
      - tejas
      - antoine
    projects:
      - laissezcloud
    resources:
      - type: preprint
        external: http://arxiv.org/abs/2501.11185

  iridescent_hotos:
    type: inprogress
    title: |
      Towards Online Code Specialization of Systems
    authors:
      - internal: vaastav
      - external: Deepak Garg
      - internal: antoine
    venue: Under submission
    year: 2025
    people:
     - vaastav
     - antoine
    projects:
      - iridescent
    resources:
      - type: preprint
        external: http://arxiv.org/abs/2501.11366

  parallax_aiops:
    type: workshop
    title: |
      Towards Using LLMs for Distributed Trace Comparison (Abstract)
    authors:
      - internal: vaastav
      - external: Pedro Las-Casas
      - external: Rodrigo Fonseca
      - internal: antoine
    venue: 6th International Workshop on Cloud Intelligence / AIOps (AIOps '25)
    year: 2025
    people:
      - vaastav
      - antoine
    resources:
      - type: preprint
        external: https://vaastavanand.com/assets/pdf/parallax_aiops25.pdf

  splitsim:
    type: inprogress
    title: |
      SplitSim: Large-Scale Simulations for Evaluating Network Systems Research
    authors:
      - internal: hejing
      - external: Praneeth Balasubramanian
      - internal: marvin
      - external: Jialin Li
      - internal: antoine
    venue: Under submission
    year: 2024
    people:
      - hejing
      - marvin
      - antoine
    projects:
      - splitsim
    resources:
      - type: preprint
        external: https://arxiv.org/abs/2402.05312

  virtuoso:
    type: inprogress
    title: |
      Virtuoso: High Resource Utilization and μs-scale Performance Isolation in
      a Shared Virtual Machine TCP Network Stack
    authors:
      - internal: matheus
      - internal: liam
      - external: Simon Peter
      - internal: antoine
    venue: Under submission
    year: 2024
    people:
      - matheus
      - liam
      - antoine
    projects:
      - virtuoso
    resources:
      - type: preprint
        external: https://arxiv.org/abs/2309.14016

  kugelblitz:
    type: inprogress
    title: |
      Kugelblitz: Streamlining Reconfigurable Packet Processing Pipeline Design
      and Evaluation
    authors:
      - internal: artemio
      - internal: antoine
    venue: Under Submission
    year: 2024
    people:
      - artemio
      - antoine
    projects:
      - kugelblitz
    resources:
      - type: preprint
        external: https://arxiv.org/abs/2305.08435

  columbo_socc:
    type: inprogress
    title: |
      Columbo: Low Level End-to-End System Traces through Modular Full-System Simulation 
    authors:
      - internal: jakob
      - internal: vaastav
      - internal: hejing
      - external: Jialin Li
      - internal: antoine
    venue: Under Submission
    year: 2024
    people:
      - jakob
      - vaastav
      - hejing
      - antoine
    projects:
      - columbo
    resources:
      - type: preprint
        external: https://arxiv.org/abs/2408.05251


  cygnus:
    type: inprogress
    title: |
      Protected Data Plane OS Using Memory Protection Keys and Lightweight
      Activation
    authors:
      - external: Yihan Yang
      - external: Zhuobin Huang
      - internal: antoine
      - external: Jialin Li
    venue: Under Submission
    year: 2023
    people:
      - antoine
    resources:
      - type: preprint
        external: https://arxiv.org/abs/2302.14417

  columbo:
    type: thesis
    title: |
      Low-Level End-to-End System-Traces through Modular Full System Simulation
    authors:
      - internal: jakob
    venue: MSc thesis
    year: 2024
    month: 5
    location: Saarbrücken, Germany
    people:
      - jakob
    projects:
      - columbo
    resources:
      - type: paperpdf
        external: https://simbricks.github.io/documents/columbo-thesis.pdf

  iridescent_src:
    type: poster
    title: |
      Online Specialization of Systems with Iridescent
    authors:
      - internal: vaastav
    venue: ACM Student Research Competition @ SOSP 2024
    year: 2024
    people:
      - vaastav
    projects:
      - iridescent
    awards:
      - First Place in Graduate Category
    resources:
      - type: poster
        internal: iridescent_src_2024.pdf

  virtuoso_src:
    type: poster
    title: |
      Virtuoso TCP Stack: Squashing Isolation and Resource Efficiency Tradeoffs
      in Virtualized Environments
    authors:
      - internal: matheus
    venue: ACM Student Research Competition @ SOSP 2023
    year: 2023
    people:
      - matheus
    projects:
      - virtuoso
    awards:
      - First Place in Graduate Category
    resources:
      - type: poster
        internal: virtuoso_src_2023.pdf

  acdsim_thesis:
    type: thesis
    title: |
      AC/DSim: Full System Energy Estimation with Modular Simulation
    authors:
      - internal: jonas
    venue: MSc thesis
    year: 2024
    month: 12
    location: Saarbrücken, Germany
    people:
      - jonas
    projects:
      - acdsim
    resources:
      - type: paperpdf
        internal: acdsim_msc_thesis_jonas_kaufmann_2024.pdf

  e2e_thesis:
    type: thesis
    title: |
      End-to-End Network Protocol Evaluation with Modular Simulation
    authors:
      - internal: marvin
    venue: MSc thesis
    year: 2024
    month: 12
    location: Saarbrücken, Germany
    people:
      - marvin
    projects:
      - cc_eval
    resources:
      - type: paperpdf
        internal: e2e_network_evaluation_marvin_meiers.pdf

  acdsim_src:
    type: poster
    title: |
      AC/DSim: Full System Energy Estimation with Modular Simulation
    authors:
      - internal: jonas
    venue: ACM Student Research Competition @ SOSP 2023
    year: 2023
    people:
      - jonas
    projects:
      - acdsim
    awards:
      - Third Place in Graduate Category
    resources:
      - type: poster
        internal: acdsim_src_2023.pdf

  blueprint:
    type: conference
    title: |
      Blueprint: A Toolchain for Highly-Reconfigurable Microservice Applications
    authors:
      - internal: vaastav
      - external: Deepak Garg
      - internal: antoine
      - external: Jonathan Mace
    venue: 29th ACM Symposium on Operating Systems Principles
    series: SOSP
    year: 2023
    month: 10
    location: Koblenz, Germany
    people:
      - vaastav
      - antoine
    projects:
      - blueprint
    resources:
      - type: paperpdf
        external: https://vaastavanand.com/assets/pdf/anand2023blueprint.pdf
      - type: video
        external: https://www.youtube.com/watch?v=atTX2PRHY7Q
      - type: slides
        external: https://blueprint-uservices.github.io/assets/slides/sosp2023.pdf
      - type: artifact
        external: https://gitlab.mpi-sws.org/cld/blueprint/blueprint-sosp23-experiments

  splitsim_yarch:
    type: workshop
    title: |
      SplitSim: Scalable and Parallel Full Data Center System Simulations
    authors:
      - internal: hejing
      - internal: antoine
    venue: The Fifth Young Architect Workshop
    series: YArch
    year: 2023
    month: 3
    location: Vancouver BC, Canada
    people:
      - hejing
      - antoine
    projects:
      - simbricks
      - splitsim
    resources:
      - type: poster
        external: https://web.mit.edu/yarch2023/poster/77.jpg
      - type: video
        external: https://www.youtube.com/watch?v=TXZT-4uQxkg

  memdisagg_sim:
    type: workshop
    title: |
      Improving Disaggregated System Evaluation with Modular End-to-End
      Simulation
    authors:
      - external: Bin Gao
      - internal: hejing
      - external: Jialin Li
      - internal: antoine
    venue: 3rd Workshop On Resource Disaggregation and Serverless Computing
    series: WORDS
    year: 2022
    month: 11
    location: Virtual
    people:
      - hejing
      - antoine
    projects:
      - simbricks
    resources:
      - type: paperpdf
        external: https://simbricks.github.io/documents/22words_simbricks-disagg.pdf
      - type: video
        external: https://www.youtube.com/watch?v=F9kO_YSnkCQ
      - type: slides
        external: https://simbricks.github.io/documents/22words_simbricks-disagg_slides.pdf

  simbricks:
    type: conference
    title: |
      SimBricks: End-to-End Network System Evaluation with Modular Simulation
    authors:
      - internal: hejing
      - external: Jialin Li
      - internal: antoine
    venue: ACM SIGCOMM 2022 Conference
    series: SIGCOMM
    year: 2022
    month: 8
    location: Amsterdam, Netherlands
    people:
      - hejing
      - antoine
    projects:
      - simbricks
    resources:
      - type: paperpdf
        external: https://simbricks.github.io/documents/22sigcomm_simbricks.pdf
      - type: video
        external: https://www.youtube.com/watch?v=fSIcPuuI6kk
      - type: slides
        external: https://simbricks.github.io/documents/22sigcomm_simbricks_slides.pdf
      - type: artifact
        external: https://github.com/simbricks/sigcomm22-artifact

  energy_tracing:
    type: workshop
    title: |
      The Odd One Out: Energy is not like Other Metrics
    authors:
      - internal: vaastav
      - external: Zhiqiang Xie
      - internal: matheus
      - external: Roberta De Viti
      - external: Thomas Davidson
      - external: Reyahneh Karimipour
      - external: Safya Alzayat
      - external: Jonathan Mace
    venue: 1st Workshop on Sustainable Computer Systems Design and Implementation
    series: HotCarbon
    year: 2022
    month: 7
    location: La Jolla, USA
    people:
      - vaastav
      - matheus
    resources:
      - type: paperpdf
        external: https://vaastavanand.com/assets/pdf/hotcarbon22-anand.pdf
      - type: video
        external: https://youtu.be/3EWmqhah7Rs

  flextoe:
    type: conference
    title: |
      FlexTOE: Flexible TCP Offload with Fine-Grained Parallelism
    authors:
      - external: Rajath Shashidhara
      - external: Timothy Stamler
      - internal: antoine
      - external: Simon Peter
    venue: 19th USENIX Symposium on Networked Systems Design and Implementation
    series: NSDI
    year: 2022
    month: 4
    location: Renton, WA, USA
    people:
      - antoine
    projects:
      - tas
    resources:
      - type: paperpdf
        external: https://www.usenix.org/system/files/nsdi22-paper-shashidhara.pdf
      - type: video
        external: https://www.youtube.com/watch?v=0sRLk2zNrdE
      - type: slides
        external: https://www.usenix.org/system/files/nsdi22_slides_shashidhara.pdf

  kugelblitz_ws:
    type: workshop
    title: |
      Exploring Domain-Specific Architectures for Network Protocol Processing
    authors:
      - internal: artemio
      - internal: mehrshad
      - internal: antoine
    venue: 2021 Cloud@MICRO Workshop
    year: 2021
    month: 10
    location: Virtual
    people:
      - artemio
      - mehrshad
      - antoine
    projects:
      - kugelblitz
    resources:
      - type: paperpdf
        external: https://people.mpi-sws.org/~antoinek/documents/21microcloud_kugelblitz.pdf
      - type: video
        external: https://www.youtube.com/watch?v=5bUEibxxURw&t=1h31m56s

  clockwork:
    type: conference
    title: |
      Serving DNNs like Clockwork: Performance Predictability from the Bottom Up
    authors:
      - external: Arpan Gujarati
      - external: Reza Karimi
      - external: Safya Alzayat
      - internal: antoine
      - external: Ymir Vigfusson
      - external: Jonathan Mace
    venue: 14th Symposium on Operating Systems Design and Implementation
    series: OSDI
    year: 2020
    month: 11
    location: Banff, Canada
    awards:
      - Distinguished Artifact Award
    people:
      - antoine
    resources:
      - type: paperpdf
        external: https://www.usenix.org/system/files/osdi20-gujarati.pdf
      - type: video
        external: https://www.youtube.com/watch?v=wHOpY_MY57Y
      - type: slides
        external: https://www.usenix.org/sites/default/files/conference/protected-files/osdi20_slides_gujarati.pdf
      - type: artifact
        external: https://gitlab.mpi-sws.org/cld/ml/clockwork

  tas:
    type: conference
    title: |
      TCP Acceleration as an OS Service
    authors:
      - internal: antoine
      - external: Tim Stamler
      - external: Simon Peter
      - external: Naveen Kr. Sharma
      - external: Thomas Anderson
      - external: Arvind Krishnamurthy
    venue: 14th EuroSys Conference
    series: EuroSys
    year: 2019
    month: 3
    location: Dresden, Germany
    people:
      - antoine
    projects:
      - tas
    resources:
      - type: paperpdf
        external: https://people.mpi-sws.org/~antoinek/documents/19eurosys_tas.pdf
      - type: video
        external: https://se.inf.tu-dresden.de/eurosys2019/Video/27_3/Antoine%20Kaufmann.mov
      - type: slides
        external: https://tcp-acceleration-service.github.io/documents/tas_slides.pptx
      - type: artifact
        external: https://github.com/tcp-acceleration-service/tas

  multitenant_dnn:
    type: techreport
    title: |
      No DNN Left Behind: Improving Inference in the Cloud with Multi-Tenancy
    authors:
      - external: Amit Samanta
      - external: Suhas Shrinivasan
      - internal: antoine
      - external: Jonathan Mace
    venue: arXiv
    year: 2019
    month: 1
    people:
      - antoine
    resources:
      - type: preprint
        external: https://arxiv.org/abs/1901.06887

  floem:
    type: conference
    title: |
      Floem: Language, Compiler, and Runtime for Network Applications on
      Heterogeneous Systems
    authors:
      - external: Phitchaya Mangpo Phothilimthana
      - external: Ming Liu
      - internal: antoine
      - external: Simon Peter
      - external: Rastislav Bodik
      - external: Thomas Anderson
    venue: 13th Symposium on Operating Systems Design and Implementation
    series: OSDI
    year: 2018
    month: 10
    location: Carlsbad, CA, USA
    people:
      - antoine
    resources:
      - type: paperpdf
        external: https://www.usenix.org/system/files/osdi18-phothilimthana.pdf
      - type: audio
        external: https://2459d6dc103cb5933875-c0245c5c937c5dedcca3f1764ecc9b2f.ssl.cf2.rackcdn.com/osdi18/phothilimthana.mp3
      - type: slides
        external: https://www.usenix.org/sites/default/files/conference/protected-files/osdi18_slides_phothilimthana.pdf
      - type: artifact
        external: https://github.com/mangpo/floem
