---
title: TCP Acceleration
people:
  - matheus
  - liam
description: |
  Fast and efficient drop-in API and protocol compatible TCP processing for
  multi-tenant data centers.
---

[TAS](https://tcp-acceleration-service.github.io/) is a drop-in highly CPU
efficient and scalable TCP acceleration service for multi-tenant environments
running on modern multi-cores. TAS runs TCP processing in an optimized fast-path
running on dedicated cores separate from the application. Processing is fully
isolated from the application, enabling better performance without sacrificing
protection.

In [FlexTOE](https://tcp-acceleration-service.github.io/FlexTOE/), we have
adapted the software-only TAS prototype to instead offload the fast-path onto a
SmartNIC to reduce processor load and improve efficiency.