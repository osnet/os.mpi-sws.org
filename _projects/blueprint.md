---
title: Blueprint
logo: projects/blueprint.png
people:
  - vaastav
description: |
  Generator for creating re-configurable microservice applications.
---

Blueprint is a framework for developing flexible and modular microservice
applications. Blueprint provides a programming abstraction for writing
microservice applications that makes it easy to later change aspects related to
the system's scaffolding and topology -- e.g. concerns such as RPC frameworks or
backends to use, placement and load balancing, replication, and so on. Using
Blueprint, applications don't early-bind to those choices. Later on, it's easy
to change any of these aspects and re-generate a fully functional variant of the
application. Blueprint makes comparative evaluation trivially easy -- it helps
developers empirically pick the best-performing libraries, frameworks, and
backends; and it helps researchers perform rigorous comparative evaluation of
new prototypes. 
