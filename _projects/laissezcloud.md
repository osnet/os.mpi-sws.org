---
title: LaissezCloud
logo: projects/laissezcloud.svg
people:
  - tejas
description: |
  Cloud-client codesign in accelerator-rich resource-constrained neoclouds
---

LaissezCloud is a redesigned framework for better client-cloud codesign in 
accelerator-rich resource-constrained deployments. Specialized applications 
and specialized hardwares require nuanced allocations --- which are poorly 
captured in today's coarse grained cloud APIs. This project capitalizes on 
dynamic application-infrastructure-cloud affinity at runtime, combining the 
needs of tenants with the wants of operators and the availabilities of the 
cloud.