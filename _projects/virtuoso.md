---
title: Virtuoso
logo: projects/virtuoso.svg
people:
  - matheus
  - liam
parent: tas
description: |
  Efficient container and VM TCP processing by elastically sharing a network
  stack, while ensuring isolation through fine-grained scheduling.
---

Virtuoso is a TCP acceleration service for virtualized environments targeted at
applications that require low latency and high throughput. Virtuoso runs as a
service alongside the host and provides a fast-path for common send and receive
operations that reduces the virtualization overheads incurred by the hypervisor
and guest operating system. The network stack is shared between multiple VMs,
thus increasing the utilization of the underlying resources. Virtuoso only has
to provision for peak aggregate utilization, instead of the sum of the peak
utilizations of each VM. Furthermore, Virtuoso ensures performance isolation in
a shared stack by doing fine-grained scheduling that reduces inter-VM
interference of running applications.