---
title: Kugelblitz
logo: projects/kugelblitz.png
people:
  - artemio
description: |
  Systematic HW design space exploration for programmable packet processing
  pipelines.
---

Kugelblitz is a framework for exploring the design space of programmable
hardware protocol processing architectures. Specifically it enables comparing
quantitative overall system performance, energy consumption, chip area, and
flexiblity for different hardware architectures with varying degrees of
programmability. Kugeblitz separates abstract protocol representations from
concrete hardware configurations, and contains tools for building concrete RTL
implementations from hardware configurations, simulating them, and compiling
abstract protocols for specific hardware configurations. We then use ASIC
synthesis tools to get the area/timing numbers, and end-to-end simulation with
SimBricks for full system performance measurements.
