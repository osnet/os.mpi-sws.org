---
title: SimBricks
subtitle: Modular End-to-End System Simulation
order: 3
people:
  - hejing
  - jonas
  - jakob
  - marvin
logo: projects/simbricks.svg
description: |
  Enable end-to-end system evaluation when hardware testbeds are out of reach
  through modular combination of existing simulators for different components.
---

[SimBricks](https://simbricks.github.io/) is a simulation framework that enables
full end-to-end evaluation of modern network systems in simulation. Our primary
aim is to to enable network systems research and instruction — from rapid
prototyping to meaningful performance evaluation. SimBricks modularly combines
and connects multiple battle-tested simulators for different components:
machines (e.g. QEMU, gem5), hardware components (e.g. Verilator, Tofino, FEMU
SSD), and networks (e.g. ns-3, OMNeT++). SimBricks assembles multiple instances
of these simulators into simulated testbeds capable of running unmodified full
system stacks, including applications and operating systems such as Linux.