---
title: Improving TAS Large-Flow Performance
people:
  - liam
parent: tas
description: |
  Optimize bulk connection TCP performance while minimizing penalties for small
  RPC latency and throughput.
---

TAS was originally primarily geared towards handling intensive RPC workloads
with a large number of connections and small packet sizes. The current version
of TAS lacks several optimizations that cater to larger flow workloads such as
bandwidth-intensive data transfers, features that are notably present in Linux
and other networking stacks (e.g., TCP Segmentation Offload, Large Receive
Offload, etc.). The objective of this project is to investigate TAS performance
with large flows, recognize its limitations, and iteratively update the design
to rectify these weaknesses. Our ideal outcome is to gain insights that would
contribute towards a refined design of TAS that can adeptly manage a wider
variety of workloads efficiently, without compromising on performance for
RPC-centric workloads.